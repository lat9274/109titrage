/*
** 109titrage.c for c in /home/wu_d/test
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Fri Apr  5 14:48:04 2013 mauhoi wu
** Last update Fri Apr  5 21:31:16 2013 mauhoi wu
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

char		*g_cmd[] =
  {
    "set title \"109titrage\"",
    "plot 'data.temp'",
    NULL
  };

enum {
  TITLE = 0,
  PLOT
};

int		trace(char degres)
{
  double	xvals[5] =
    {0, 5.0, 10.0, 15.0, 20.0};
  double	yvals[7] =
    {0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0};
  FILE		*temp;
  FILE		*gppipe;
  int		i;
  char		buff[1000];
  char		vstr[64];
  int		vi;

  if (read(0, buff, 1000) == -1)
    return (-1);
  if (!(temp = fopen("data.temp", "w")))
    return (-1);
  if (!(gppipe = popen("gnuplot -persistent", "w")))
    return (-1);
  for (i = 0; buff[i] != 'F' && buff[i + 1] != 'I' && buff[i + 2] != 'N'; i++)
    {
      vstr[0] = '\0';
      vi = 0;
      while (buff[i] != ';' && buff[i] != '\n')
	{
	  vstr[vi] = buff[i];
	  vi++;
	  i++;
	}
      vstr[vi] = '\0';
      fprintf(temp, vstr);
      fprintf(temp, " ");
      if (buff[i] == '\n')
	fprintf(temp, "\n");
    }
  for (i = 0; i < 2; i++)
    {
      fprintf(gppipe, "%s \n", g_cmd[i]);
    }
  return (0);
}

int		main(int ac, char **av)
{
  if (ac == 1)
    {
      if (trace(0))
	fprintf(stderr, "fail\n");
    }
  else if (ac == 2)
    {
      if (trace(atoi(av[1])))
	fprintf(stderr, "fail\n");
    }
  return (EXIT_SUCCESS);
}
