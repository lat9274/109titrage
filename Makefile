##
## Makefile for Makefile in /home/wu_d/test
## 
## Made by mauhoi wu
## Login   <wu_d@epitech.net>
## 
## Started on  Fri Apr  5 20:20:01 2013 mauhoi wu
## Last update Fri Apr  5 21:28:46 2013 mauhoi wu
##

NAME=	109titrage

SRC=	109titrage.c

OBJ=	$(SRC:.c=.o)

CC=	cc

RM=	rm -vf

CFLAG +=	-Wall -Wextra -O2

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ)

all: $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all
